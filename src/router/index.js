import Vue from 'vue'
import Router from 'vue-router'
import main from '@/components/main'
import datepicker from '@/components/datepicker/datepicker_demo'
import slideshow from '@/components/slideshow/slideshow_demo'
import cityganged from '@/components/cityganged/cityganged_demo'
import pagination from '@/components/pagination/pagination_demo'
import timePicker from '@/components/timePicker/timePicker_demo'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: main,
      children:[
        {
          path: '/datepicker',
          name: 'datepicker',
          component: datepicker,
          meta: {
            title:"DatePicker 日期选择器"
          },
        },
        {
          path: '/slideshow',
          name: 'slideshow',
          component: slideshow,
          meta: {
            title:"slideshow 轮播图组件"
          },
        },
        {
          path: '/cityganged',
          name: 'cityganged',
          component: cityganged,
          meta: {
            title:"cityganged 联动"
          },
        },
        {
          path: '/pagination',
          name: 'pagination',
          component: pagination,
          meta: {
            title:"pagination 分页"
          },
        },
        {
          path: '/timePicker',
          name: 'timePicker',
          component: timePicker,
          meta: {
            title:"timePicker 时间选择器"
          },
        },
      ]
    },
    
  ]
})
